# SmarterD Dashboard Automation

## Install

```
- npm install
```

## Running Test Suite

```
- npm run test ( to run e2e )
- npm run security ( to run security module )
- npm run security-dashboard ( to run security dashboard )
- npm run security-roadmap ( to run security roadmap )
- npm run security-program ( to run security program )
- npm run asset ( to run asset module )
- npm run capability ( to run capability module )
- npm run finance ( to run finance module )
```

## Reports

```
-  npm run serve
```

## Configure

Configuring for different browsers

browsers.ts add chromium (or) firefox (or) webkit
