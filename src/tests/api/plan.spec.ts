const frisby = require("frisby");
const Joi = frisby.Joi;
const BASE_URL = "https://www.smarterd.com/api/security/";
const NEW_URL = "http://localhost:8080/smarterd/api/";

// Do setup first
frisby.globalSetup({
  request: {
    headers: {
      "Content-Type": "application/json",
    },
  },
});

/**
 * get plans
 */
describe(`plan api`, () => {
  test(`get:plan`, async () => {
    let url = NEW_URL + "plans/demo";
    return await frisby
      .get(url)
      .expect("status", 200)
      .expect("json", "status", true)
      .expect("json", "total", 42)
      .expect("json", "data.0", {
        id: "5d5c5a6b237b3b55b5e6a87d",
        name: "Use a Passive Asset Discovery",
        planType: "CS",
        desc: "Test",
      });
  });

  /**
   * get sub plans
   */
  describe(`sub plan api`, () => {
    test(`get:subplan`, async () => {
      let url = NEW_URL + "subplans/demo/5efd0ccf255c5521c09e2208";
      return await frisby
        .get(url)
        .expect("status", 200)
        .expect("json", "status", true)
        .expect("json", "total", 2);
    });
  });
});
