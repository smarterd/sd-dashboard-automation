import { BrowserTypes } from "../../browsers"
import { PlayWrightHelper } from "../../helpers"

const playwright = require("playwright")
const fs = require("fs")

const data = JSON.parse(fs.readFileSync("data.json"))
const tag = JSON.parse(fs.readFileSync("tags.json"))

// Loop over all the supported browsers
for (const browserType of BrowserTypes) {
  describe(`(${browserType}):security`, () => {
    let PAGE_URL = data["environment"]
    let browser = null
    let page = null
    /**
     * Create the browser and page context
     */
    beforeAll(async () => {
      browser = await playwright[browserType].launch({
        headless: data["headless"],
        slowMo: data["delay"],
      })
      page = await browser.newPage()

      if (!page) {
        throw new Error("Connection wasn't established")
      }

      // Open the page
      await page.goto(PAGE_URL, {
        waitUntil: "networkidle0",
      })
    }, 50000)
    /**
     * Runs after each test
     */
    beforeEach(async () => {
      //await PlayWrightHelper.screenshot(jasmine, page, null, "-beforeEach")
    })

    /**
     * Runs after each test
     */
    afterEach(async () => {
      await PlayWrightHelper.screenshot(jasmine, page, null, "")
    })

    afterAll(async () => {
      await browser.close()
    })

    /**
     * login to the application
     */
    test(`(${browserType}):login`, async (done) => {
      await page.fill(tag["login"]["email"], data["login"]["email"])
      await page.fill(tag["login"]["password"], data["login"]["password"])
      await page.fill(tag["login"]["company"], data["login"]["company"])
      await page.click(tag["login"]["loginButton"])
      done()
    }, 100000)
    /**
     * click on security on the navigation panel
     */
    test(`(${browserType}):click-security-side-panel`, async () => {
      await page.click(tag["navigation"]["security"])
    })
    /**
     * click on the program in security side panel drop down
     */
    test(`(${browserType}):click-program-security-side-panel`, async () => {
      await page.click(tag["navigation"]["securityProgram"])
    })
    /**
     *
     */
    test(`(${browserType}):click-programlist-program`, async () => {
      await page.click(tag["program"]["listview"])
    })

    /**
     * click on asset on the navigation panel
     */
    test(`(${browserType}):click-security-side-panel`, async () => {
      await page.click(tag["navigation"]["asset"])
    })
    /**
     * click on the asset detail in asset side panel drop down
     */
    test(`(${browserType}):click-program-security-side-panel`, async () => {
      await page.click(tag["navigation"]["assetDetail"])
    })

    /**
     * click on capability on the navigation panel
     */
    test(`(${browserType}):click-capability-side-panel`, async () => {
      await page.click(tag["navigation"]["capability"])
    })

    /**
     * click on finance on the navigation panel
     */
    test(`(${browserType}):click-finance-side-panel`, async () => {
      await page.click(tag["navigation"]["finance"])
    })

    /**
     * logout
     */
    test(`(${browserType}):logout`, async () => {
      await page.click(tag["logout"])
    })
  })
}
