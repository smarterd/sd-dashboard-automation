import { BrowserTypes } from "../../../browsers"
import { PlayWrightHelper } from "../../../helpers"
import { Utils } from "../common"
import * as Constants from "../../../constants"

const playwright = require("playwright")
const fs = require("fs")

const data = JSON.parse(fs.readFileSync("data.json"))
const tag = JSON.parse(fs.readFileSync("tags.json"))



// Loop over all the supported browsers
for (const browserType of BrowserTypes) {
  describe(`(${browserType}):security`, () => {
    let PAGE_URL = data["environment"]
    let browser = null
    let page = null
    /**
     * Create the browser and page context
     */
    beforeAll(async () => {
      browser = await playwright[browserType].launch({
        headless: data["headless"],
        slowMo: data["delay"],
      })
      page = await browser.newPage()

      if (!page) {
        throw new Error("Connection wasn't established")
      }

      // Open the page
      await page.goto(PAGE_URL, {
        waitUntil: "networkidle0",
      })
    }, 50000)
    /**
     * Runs after each test
     */
    beforeEach(async () => {
      //await PlayWrightHelper.screenshot(jasmine, page, null, "-beforeEach")
    })

    /**
     * Runs after each test
     */
    afterEach(async () => {
      await PlayWrightHelper.screenshot(jasmine, page, null, "")
    })

    afterAll(async () => {
      await browser.close()
    })

    /**
     * login to the application
     */
    test(`(${browserType}):login`, async (done) => {
      Utils.login(page, done, tag, data)
    })
    /**
     * click on security on the navigation panel
     */
    test(`(${browserType}):click-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY)
    })

    /**
     * click on roadmap in security panel dropdown
     */

    test(`(${browserType}):click-roadmap-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_ROADMAP)
    })

    /**
     * Function for selecting dropdowns in roadmap status filter
     * dropdownName: the name of the dropdown being clicked in the status filter
     * arrayNum: the number of the dropdown- 'approved'=1, 'New'=2 etc.
     */
    function statusFilterTest(dropdownName, arrayNum){
      test(`(${browserType}):click-roadmap-status-filter${dropdownName}`, async (done) => {
        //click on the element for dropdown to be visible
        await page.click(Constants.SECURITY_ROADMAP_STATUS_FILTER)
        //identify the drop down element
        const statusFilter = await page.$(
          Constants.SECURITY_ROADMAP_STATUS_FILTER
        )
    
        // selecting 'dropdownName' from dropdown
        const statusValue = await statusFilter.$eval(
          "xpath=//ul/li[@class='k-item']["+arrayNum+"]",
          (element) => {
            element.click()
            return element.innerText
          }
        )
        console.log("value of drop down selected " + statusValue)
        done()
      })
    }


    /**
     * Function for selecting dropdowns in roadmap multiselect
     * dropdownName: the name of the dropdown being clicked in the dropdown
     * arrayNum: the number of the dropdown- 'AccountMonitoringAndControl'=1, etc.
     */
    function multiSelectTest(dropdownName, arrayNum){
      test(`(${browserType}):click-roadmap-multi-select-filter${dropdownName}`, async (done) => {
        //click on the element for dropdown to be visible
        await page.click(Constants.SECURITY_ROADMAP_MULTI_SELECT)
        //identify the drop down element
        const statusFilter = await page.$(
          Constants.SECURITY_ROADMAP_MULTI_SELECT
        )
    
        // selecting 'dropdownName' from dropdown
        const statusValue = await statusFilter.$eval(
          "xpath=//ul/li[@class='k-item']["+arrayNum+"]",
          (element) => {
            element.click()
            return element.innerText
          }
        )
        console.log("value of drop down selected " + statusValue)
        done()
      })
    }
    

  


    // test for approved dropdown
    statusFilterTest('Approved', 1)
    // test for new dropdown
    statusFilterTest('New', 2)
    // test for backlog dropdown
    statusFilterTest('Backlog', 3)
    // test for plan w/o capabilities dropdown
    statusFilterTest('PlanWithoutCapability', 4)
    // test for capabilities w/o plan dropdown
    statusFilterTest('CapabilitiesWithoutPlan', 5)

    



    /**
     * click on security roadmap year filter
     */
    test(`(${browserType}):click-roadmap-year-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_YEAR_FILTER
      )
    })

    test(`(${browserType}):click-roadmap-year-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_YEAR_FILTER
      )
    })


    test(`(${browserType}):click-roadmap-year-filter-FY21`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_ROADMAP_YEAR_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_ROADMAP_YEAR_FILTER
      )
  
      // selecting 'FY21' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })
  





  

    /**
     * click on security roadmap refresh
     */
    test(`(${browserType}):click-roadmap-refresh`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_REFRESH
      )
    })

    /**
     * Click on roadmap multi select
     */
    test(`(${browserType}):click-roadmap-Multi-Select`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_MULTI_SELECT
      )
    })

    //Used to close the multiselect dropdown
    test(`(${browserType}):click-roadmap-Multi-Select`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_MULTI_SELECT
      )
    })

    // Tests for multiselect options
    multiSelectTest('AccountMonitoringAndControl', [1])
    multiSelectTest('ApplicationSoftwareCapability', [2])
    multiSelectTest('BoundaryDefense', [3])
    multiSelectTest('ContinuousVulnerabilityManagment', [4])
    multiSelectTest('ControlledAccessBasedOnTheNeedToKnow', [5])
    multiSelectTest('ControlledUseOfAdministrativePrivelages', [6])
    multiSelectTest('DataProtection', [7])


    /**
     * logout
     */
    test(`(${browserType}):logout`, async (done) => {
      Utils.logout(page, done, tag)
    })
  })
}
