import { BrowserTypes } from "../../../browsers"
import { PlayWrightHelper } from "../../../helpers"
import { Utils } from "../common"
import * as Constants from "../../../constants"

const playwright = require("playwright")
const fs = require("fs")

const data = JSON.parse(fs.readFileSync("data.json"))
const tag = JSON.parse(fs.readFileSync("tags.json"))

// Loop over all the supported browsers
for (const browserType of BrowserTypes) {
  describe(`(${browserType}):security`, () => {
    let PAGE_URL = data["environment"]
    let browser = null
    let page = null
    /**
     * Create the browser and page context
     */
    beforeAll(async () => {
      browser = await playwright[browserType].launch({
        headless: data["headless"],
        slowMo: data["delay"],
      })
      page = await browser.newPage()

      if (!page) {
        throw new Error("Connection wasn't established")
      }

      // Open the page
      await page.goto(PAGE_URL, {
        waitUntil: "networkidle0",
      })
    }, 50000)
    /**
     * Runs after each test
     */
    beforeEach(async () => {
      //await PlayWrightHelper.screenshot(jasmine, page, null, "-beforeEach")
    })

    /**
     * Runs after each test
     */
    afterEach(async () => {
      await PlayWrightHelper.screenshot(jasmine, page, null, "")
    })

    afterAll(async () => {
      await browser.close()
    })

    /**
     * login to the application
     */
    test(`(${browserType}):login`, async (done) => {
      Utils.login(page, done, tag, data)
    }, 100000)

    /**
     * click on security on the navigation panel
     */

    test(`(${browserType}):click-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY)
    })

    /**
     * click on the program in security side panel drop down
     */

    test(`(${browserType}):click-program-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_PROGRAM)
    })

    /**
     * click program detailed view
     */

    test(`(${browserType}):click-program-list-program`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_PROGRAM_LIST)
    })

    /**
     * click program new
     */

    test(`(${browserType}):click-program-new`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_PROGRAM_NEW)
    })

    /**
     * click program plan name
     */

    test(`(${browserType}):click-program-plan-name`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_NAME
      )
    })

    test(`(${browserType}):click-program-plan-name-fill`, async (done) => {
      PlayWrightHelper.elementFill(
        page,
        Constants.SECURITY_PROGRAM_PLAN_NAME,
        "Test 1"
      )
      done()
    })

    /**
     * click program plan description
     */

    test(`(${browserType}):click-program-plan-description`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DESC
      )
    })

    /**
     * click program plan owner
     */

    test(`(${browserType}):click-program-plan-owner`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_OWNER
      )
    })

    /**
     * click program plan PM
     */

    test(`(${browserType}):click-program-plan-PM`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PM
      )
    })

    /**
     * click program plan assignee
     */

    test(`(${browserType}):click-program-plan-assignee`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_ASSIGNEE
      )
    })

   

    /**
     * click program plan category
     */

    test(`(${browserType}):click-program-plan-category`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_CATEGORY
      )
    })
    test(`(${browserType}):click-program-plan-category`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_CATEGORY
      )
    })


    /**
     * Function for selecting dropdowns in program plan category
     * dropdownName: the name of the dropdown being clicked in the dropdown
     * arrayNum: the number of the dropdown- 'compliance mitigation'=1, etc.
     */
    function programPlanCategoryTest(dropdownName, arrayNum){
      test(`(${browserType}):click-program-plan-category-${dropdownName}`, async (done) => {
        //click on the element for dropdown to be visible
        await page.click(Constants.SECURITY_PROGRAM_PLAN_CATEGORY)
        //identify the drop down element
        const statusFilter = await page.$(
          Constants.SECURITY_PROGRAM_PLAN_CATEGORY
        )
    
        // selecting 'dropdownName' from dropdown
        const statusValue = await statusFilter.$eval(
          "xpath=//ul/li[@class='k-item']["+arrayNum+"]",
          (element) => {
            element.click()
            return element.innerText
          }
        )
        console.log("value of drop down selected " + statusValue)
        done()
      })
    }
    //programPlanCategoryTest('capability-enhancment', 0)
    programPlanCategoryTest('complaince-mitigation', 1)
    programPlanCategoryTest('new-business-capability', 2)
    programPlanCategoryTest('new-CS-capability', 3)
    programPlanCategoryTest('technology-refresh', 4)
    programPlanCategoryTest('vulnerability-managment', 5)





    /**
     * click program plan cross functional team
     */

    test(`(${browserType}):click-program-plan-cross_funtional_team`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_CFT
      )
    })
    /**
     * click program plan cross functional team
     */

    test(`(${browserType}):click-program-plan-cross_funtional_team`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_CFT
      )
    })

    /**
     * Function for selecting dropdowns in program plan category
     * dropdownName: the name of the dropdown being clicked in the dropdown
     * arrayNum: the number of the dropdown- 'compliance mitigation'=1, etc.
     */
    function programPlanCFTTest(dropdownName, arrayNum){
      test(`(${browserType}):click-program-plan-category-${dropdownName}`, async (done) => {
        //click on the element for dropdown to be visible
        await page.click(Constants.SECURITY_PROGRAM_PLAN_CFT)
        //identify the drop down element
        const statusFilter = await page.$(
          Constants.SECURITY_PROGRAM_PLAN_CFT
        )
    
        // selecting 'dropdownName' from dropdown
        const statusValue = await statusFilter.$eval(
          "xpath=//ul/li[@class='k-item']["+arrayNum+"]",
          (element) => {
            element.click()
            return element.innerText
          }
        )
        console.log("value of drop down selected " + statusValue)
        done()
      })
    }

    //programPlanCFTTest('BIZ-Unit1', 0)
    programPlanCFTTest('BIZ-Unit2', 1)
    programPlanCFTTest('Communications', 2)



    /**
     * click program plan status
     */

    test(`(${browserType}):click-program-plan-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_STATUS
      )
    })
    /**
     * click program plan status
     */

    test(`(${browserType}):click-program-plan-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_STATUS
      )
    })

    /**
     * click program plan status yellow
     */
    test(`(${browserType}):click-program-plan-status-yellow`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_STATUS
      )
  
      // selecting 'physical security' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan status red
     */
    test(`(${browserType}):click-program-plan-status-red`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_STATUS
      )
  
      // selecting 'physical security' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })



    
 /**
     * click program plan type
     */

    test(`(${browserType}):click-program-plan-type`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_TYPE
      )
    })
    /**
     * click program plan type
     */
    test(`(${browserType}):click-program-plan-type`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_TYPE
      )
    })

    /**
     * click program plan type physical security
     */
    test(`(${browserType}):click-program-plan-type-physical-security`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_TYPE)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_TYPE
      )
  
      // selecting 'physical security' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan type cyber physical security
     */
    test(`(${browserType}):click-program-plan-type-cyber-physical-security`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_TYPE)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_TYPE
      )
  
      // selecting 'cyber physical security' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan type information technology
     */
    test(`(${browserType}):click-program-plan-type-information-technology`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_TYPE)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_TYPE
      )
  
        
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][3]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })
  

    /**
     * click program plan priority
     */

    test(`(${browserType}):click-program-plan-priority`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PRIORITY
      )
    })

    /**
     * click program plan priority
     */

    test(`(${browserType}):click-program-plan-priority`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PRIORITY
      )
    })

    /**
     * click program plan priority low
     */
    test(`(${browserType}):click-program-plan-priority-low`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_PRIORITY)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_PRIORITY
      )
  
         
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan priority medium
     */
    test(`(${browserType}):click-program-priority-medium`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_PRIORITY)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_PRIORITY
      )
  
         
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan size
     */

    test(`(${browserType}):click-program-plan-size`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SIZE
      )
    })

    /**
     * click program plan size
     */

    test(`(${browserType}):click-program-plan-size`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SIZE
      )
    })

    /**
     * click program plan size medium
     */
    test(`(${browserType}):click-program-plan-size-medium`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SIZE)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SIZE
      )
  
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan size small
     */
    test(`(${browserType}):click-program-plan-size-small`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SIZE)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SIZE
      )
  
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan size x-large
     */
    test(`(${browserType}):click-program-plan-size-x-large`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SIZE)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SIZE
      )
  
       
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][3]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })




    /**
    * click program plan Planned Start Date
    

   test(`(${browserType}):click-program-plan-PSD`, async (done) => {
    PlayWrightHelper.elementClick(
      page,
      done,
      Constants.SECURITY_PROGRAM_PLAN_PSD
    )
  })

  test(`(${browserType}):fill-program-plan-PSD`, async (done) => {
    PlayWrightHelper.elementFill(
      page,
      Constants.SECURITY_PROGRAM_PLAN_PSD,
      '07//02/2020'
    )
  })

  */

    /**
     * click program plan Planned End Date
     */

    test(`(${browserType}):click-program-plan-PED`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PED
      )
    })

    /**
     * click program plan duration
     */

    test(`(${browserType}):click-program-plan-duration`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DURATION
      )
    })

    test(`(${browserType}):click-program-plan-save`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SAVE
      )
    })

    test(`(${browserType}):click-program-plan-save-ok`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DAILOG_SAVE_OK
      )
    })

    

    //Edit has been enabled
    test(`(${browserType}):click-program-plan-edit`, async (done) => {
      await page.click(
        'xpath=//*[@id="plans"]/kendo-grid/div/kendo-grid-list/div/div[1]/table/tbody/tr[1]'
      )
      done()
    })
     
    test(`(${browserType}):click-program-plan-comment`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_COMMENT

      )
    })

    test(`(${browserType}):click-program-plan-save`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SAVE

      )
    })

    test(`(${browserType}):click-program-plan-save-ok2`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DAILOG_SAVE_OK
      )
    })

    //Edit has been enabled
    test(`(${browserType}):click-program-plan-edit`, async (done) => {
      await page.click(
        'xpath=//*[@id="plans"]/kendo-grid/div/kendo-grid-list/div/div[1]/table/tbody/tr[1]'
      )
      done()
    })


    //Subplan testing

    /**
     * Click dropdown to create subplan
     */
    test(`(${browserType}):click-program-plan-row`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_ROW_ADD
      )
    })
    /**
     * Create sub plan
     */
    test(`(${browserType}):click-program-plan-row-add`, async (done) => {
      await page.click(
        'xpath=//*[@id="plans"]/kendo-grid/div/kendo-grid-list/div/div[1]/table/tbody/tr[0]'
      )
      done()
    })

    /**
     * click plan subplan name
     */
    test(`(${browserType}):click-program-plan-subplan-name`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_NAME
      )
    })

    test(`(${browserType}):click-program-plan-subplan-name-fill`, async (done) => {
      PlayWrightHelper.elementFill(
        page,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_NAME,
        "Subplan Test 1"
      )
      done()
    })

    /**
     * click program plan subplan owner
     */

    test(`(${browserType}):click-program-plan-subplan-owner`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_OWNER
      )
    })

    /**
     * click program plan subplan PM
     */

    test(`(${browserType}):click-program-plan-subplan-PM`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PM
      )
    })

    /**
     * click program plan subplan assignee
     */

    test(`(${browserType}):click-program-plan-subplan-assignee`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_ASSIGNEE
      )
    })


    /**
     * click program plan subplan cross functional team
     */

    test(`(${browserType}):click-program-plan-subplan-cross-funtional-team`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_CFT
      )
    })
    /**
     * click program plan subplan cross functional team
     */

    test(`(${browserType}):click-program-plan-subplan-cross-funtional-team`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_CFT
      )
    })

    /**
     * Function for selecting dropdowns in program plan subplan category
     * dropdownName: the name of the dropdown being clicked in the dropdown
     * arrayNum: the number of the dropdown- 'compliance mitigation'=1, etc.
     */
    function programPlanSubplanCFTTest(dropdownName, arrayNum){
      test(`(${browserType}):click-program-plan-subplan-category-${dropdownName}`, async (done) => {
        //click on the element for dropdown to be visible
        await page.click(Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_CFT)
        //identify the drop down element
        const statusFilter = await page.$(
          Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_CFT
        )
    
        // selecting 'dropdownName' from dropdown
        const statusValue = await statusFilter.$eval(
          "xpath=//ul/li[@class='k-item']["+arrayNum+"]",
          (element) => {
            element.click()
            return element.innerText
          }
        )
        console.log("value of drop down selected " + statusValue)
        done()
      })
    }

    //programPlanSubplanCFTTest('BIZ-Unit1', 0)
    programPlanSubplanCFTTest('BIZ-Unit2', 1)
    programPlanSubplanCFTTest('Communications', 2)



    /**
     * click program plan subplan status
     */

    test(`(${browserType}):click-program-plan-subplan-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS
      )
    })
    /**
     * click program plan subplan status
     */

    test(`(${browserType}):click-program-plan-subplan-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS
      )
    })

    /**
     * click program plan subplan status yellow
     */
    test(`(${browserType}):click-program-plan-subplan-status-yellow`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS
      )
  
      // selecting 'physical security' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan subplan status red
     */
    test(`(${browserType}):click-program-plan-subplan-status-red`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS
      )
  
      // selecting 'physical security' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })



    
     
    

    
    /**
     * click program plan priority
     */

    test(`(${browserType}):click-program-plan-priority`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PRIORITY
      )
    })

    /**
     * click program plan subplan priority
     */

    test(`(${browserType}):click-program-plan-subplan-priority`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PRIORITY
      )
    })

    /**
     * click program plan subplan priority low
     */
    test(`(${browserType}):click-program-plan-subplan-priority-low`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PRIORITY)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PRIORITY
      )
  
         
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click program plan subplan priority medium
     */
    test(`(${browserType}):click-program-priority-subplan-medium`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PRIORITY)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PRIORITY
      )
  
         
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

  


  
  /**
   * click program plan subplan Planned Start Date
   */
    

   test(`(${browserType}):click-program-plan-subplan-PSD`, async (done) => {
    PlayWrightHelper.elementClick(
      page,
      done,
      Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PSD
    )
  })

  // test(`(${browserType}):fill-program-plan-subplan-PSD`, async (done) => {
  //   PlayWrightHelper.elementFill(
  //     page,
  //     Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_PSD,
  //     '07//02/2020'
  //   )
  // })

  

    /**
     * click program plan sublan Planned End Date
     */

    test(`(${browserType}):click-program-plan-subplan-PED`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PED
      )
    })

    /**
     * click program plan subplan duration
     */

    test(`(${browserType}):click-program-plan-subplan-duration`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DURATION
      )
    })

    /**
     * click program plan subplan start date
     */

    test(`(${browserType}):click-program-plan-subplan-start-date`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_SD
      )
    })

    /**
     * click program plan subplan end date
     */

    test(`(${browserType}):click-program-plan-subplan-end-date`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_ED
      )
    })

    /**
     * click program plan subplan complete
     */

    test(`(${browserType}):click-program-plan-subplan-complete`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_COMPLETE
      )
    })

    /**
     * click program plan subplan details objective
     */

    test(`(${browserType}):click-program-plan-subplan-details-objective`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS_OBJECTIVE
      )
    })

    /**
     * click program plan subplan details testing procedure
     */

    test(`(${browserType}):click-program-plan-subplan-details-TP`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS_TP
      )
    })

    /**
     * click program plan subplan details guidance
     */

    test(`(${browserType}):click-program-plan-subplan-details-guidance`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS_GUIDANCE
      )
    })

    /**
     * click program plan subplan comment
     */

    test(`(${browserType}):click-program-plan-subplan-details-guidance`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_COMMENT
      )
    })



    /**
     * click program plan subplan save
     */

    test(`(${browserType}):click-program-plan-subplan-save`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SUBPLAN_SAVE
      )
    })

    test(`(${browserType}):click-program-plan-save-ok3`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DAILOG_SAVE_OK
      )
      })


    //Edit has been enabled
    test(`(${browserType}):click-program-plan-edit`, async (done) => {
      await page.click(
        'xpath=//*[@id="plans"]/kendo-grid/div/kendo-grid-list/div/div[1]/table/tbody/tr[1]'
      )
      done()
    })

    test(`(${browserType}):click-program-plan-delete`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DELETE
      )
    })

    test(`(${browserType}):click-program-plan-delete-ok`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DAILOG_DELETE_OK
      )
    })

    test(`(${browserType}):click-program-plan-save-ok4`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DAILOG_SAVE_OK
      )
    })
    

    

    

    

    

    

    /**
     * logout
     */
    test(`(${browserType}):logout`, async (done) => {
      Utils.logout(page, done, tag)
    })
  })
}
