import { BrowserTypes } from "../../../browsers"
import { PlayWrightHelper } from "../../../helpers"
import { Utils } from "../common"
import * as Constants from "../../../constants"

const playwright = require("playwright")
const fs = require("fs")

const data = JSON.parse(fs.readFileSync("data.json"))
const tag = JSON.parse(fs.readFileSync("tags.json"))

// Loop over all the supported browsers
for (const browserType of BrowserTypes) {
  describe(`(${browserType}):security`, () => {
    let PAGE_URL = data["environment"]
    let browser = null
    let page = null
    /**
     * Create the browser and page context
     */
    beforeAll(async () => {
      browser = await playwright[browserType].launch({
        headless: data["headless"],
        slowMo: data["delay"],
      })
      page = await browser.newPage()

      if (!page) {
        throw new Error("Connection wasn't established")
      }

      // Open the page
      await page.goto(PAGE_URL, {
        waitUntil: "networkidle0",
      })
    }, 50000)
    /**
     * Runs after each test
     */
    beforeEach(async () => {
      //await PlayWrightHelper.screenshot(jasmine, page, null, "-beforeEach")
    })

    /**
     * Runs after each test
     */
    afterEach(async () => {
      await PlayWrightHelper.screenshot(jasmine, page, null, "")
    })

    afterAll(async () => {
      await browser.close()
    })

    /**
     * login to the application
     */
    test(`(${browserType}):login`, async (done) => {
      Utils.login(page, done, tag, data)
    }, 100000)
    /**
     * click on security on the navigation panel
     */
    test(`(${browserType}):click-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY)
    })
    /**
     * click on security dashboard
     */
    test(`(${browserType}):click-dashboard-security`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_DASHBOARD)
    })

    /**
     * click on Control Family Status
     */

    test(`(${browserType}):click-dashboard-security-control-family-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS
      )
    })
    test(`(${browserType}):click-dashboard-security-control-family-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS
      )
    })

    /** 
     * Does not select when array number is 0 or the part is highlighted
     * 
    test(`(${browserType}):click-security-dashboard-family-status-medium`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_FAMILY_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS
      )
  
      // selecting 'FY21' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][0]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })
    */


    test(`(${browserType}):click-security-dashboard-family-status-high`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_FAMILY_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS
      )
  
      // selecting 'FY21' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    test(`(${browserType}):click-security-dashboard-family-status-critical`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_FAMILY_STATUS)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS
      )
  
      // selecting 'FY21' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })


  
    /** 
    function dropdownTest(firstClickName, dropdownName ,arrayNum)
    {
      test(`(${browserType}):click-dashboard-security-${firstClickName}-${dropdownName}`, async (done) => {
        //click on the element for dropdown to be visible
        await page.click(firstClickName)
        //identify the drop down element
        const statusFilter = await page.$(
          firstClickName
        )
    
        // selecting 'dropdownName' from dropdown
        const statusValue = await statusFilter.$eval(
          "xpath=//ul/li[@class='k-item']["+arrayNum+"]",
          (element) => {
            element.click()
            return element.innerText
          }
        )
        console.log("value of drop down selected " + statusValue)
        done()
      })
    }
    */
    



    /**
     * click on security Dashboard control risk
     */
    test(`(${browserType}):click-dashboard-security-control-risk`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTROL_RISK
      )
    })

    /**
     * click on security Dashboard control summary
     */
    test(`(${browserType}):click-dashboard-security-control-summary`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTROL_SUMMARY
      )
    })

    /**
     * click on dashboard security exposure
     */
    test(`(${browserType}):click-dashboard-security-security-exposure`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_SECURITY_EXPOSURE
      )
    })

    /**
     * click on dashboard enabling assets
     */
    test(`(${browserType}):click-dashboard-security-enabling-assets`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_ENABLING_ASSET
      )
    })

    /**
     * click on dashboard In-Scope asset
     */

    test(`(${browserType}):click-dashboard-security-In-Scope-assets`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_IN_SCOPE_ASSET
      )
    })

    /**
     * click on dashboard Budget Status Filter
     */

    test(`(${browserType}):click-dashboard-security-budget-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER
      )
    })
    /**
     * Closes the dropdown for budget status filter
     */
    test(`(${browserType}):click-dashboard-security-budget-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER
      )
    })

    /**
     * click on budget status filter staged to begin dropdown
     */
    test(`(${browserType}):click-security-dashboard-budget-status-filter-staged-to-begin`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click on budget status filter staged to complete dropdown
     */
    test(`(${browserType}):click-security-dashboard-budget-status-filter-staged-to-complete`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER
      )
  
      // selecting 'staged to complete' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })


    /**
     * click on Dashboard Budget Detail View
     */
    test(`(${browserType}):click-dashboard-security-budget-detail-view`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_BUDGET_DETAIL_VIEW
      )
    })

    /**
     * click on cross for budget detail view- leads to chart view
     */

    test(`(${browserType}):click-dashboard-security-budget-chart-view`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_BUDGET_CHART_VIEW
      )
    })

    /**
     * click on Dashboard Contract Status Filter
     */
    test(`(${browserType}):click-dashboard-security-contract-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER
      )
    })
    /**
     * click on Dashboard Contract Status Filter to close
     */
    test(`(${browserType}):click-dashboard-security-contract-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER
      )
    })

    /**
     * click on contract status filter expired dropdown
     */
    test(`(${browserType}):click-security-dashboard-contract-status-filter-expired`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click on contract status filter recent dropdown
     */
    test(`(${browserType}):click-security-dashboard-contract-status-filter-recent`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })
 
    /**
     * click on Dashboard Eol Category Filter
     */
    test(`(${browserType}):click-dashboard-security-eol-category-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_EOL_CATEGORY_FILTER
      )
    })

    /**
     * click on Dashboard Eol Category Filter
     */
    test(`(${browserType}):click-dashboard-security-eol-category-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_EOL_CATEGORY_FILTER
      )
    })

    /**
     * click on Dashboard Eol Category Filter IT-ASSET dropdown
     */
    test(`(${browserType}):click-security-dashboard-eol-category-filter-IT-ASSET`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_EOL_CATEGORY_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_EOL_CATEGORY_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })


    /**
     * click on Dashboard Eol Status Filter
     */

    test(`(${browserType}):click-dashboard-security-eol-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_EOL_STATUS_FILTER
      )
    })

    /**
     * click on Dashboard Eol Status Filter
     */

    test(`(${browserType}):click-dashboard-security-eol-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_EOL_STATUS_FILTER
      )
    })

    /**
     * click on Dashboard Eol status Filter upcoming dropdown
     */
    test(`(${browserType}):click-security-dashboard-eol-status-filter-upcoming`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_EOL_STATUS_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_EOL_STATUS_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click on Dashboard Collaboration Category Filter
     */

    test(`(${browserType}):click-dashboard-security-collaboration-category-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER
      )
    })

    /**
     * click on Dashboard Collaboration Category Filter
     */

    test(`(${browserType}):click-dashboard-security-collaboration-category-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER
      )
    })

    /**
     * click on Dashboard Collaboration Category Filter Asset
     */
    test(`(${browserType}):click-security-dashboard-collabaration-category-filter-asset`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][1]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click on Dashboard Collaboration Category Filter Control
     */
    test(`(${browserType}):click-security-dashboard-collabaration-category-filter-control`, async (done) => {
      //click on the element for dropdown to be visible
      await page.click(Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER)
      //identify the drop down element
      const statusFilter = await page.$(
        Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER
      )
  
      // selecting 'staged to begin' from dropdown
      const statusValue = await statusFilter.$eval(
        "xpath=//ul/li[@class='k-item'][2]",
        (element) => {
          element.click()
          return element.innerText
        }
      )
      console.log("value of drop down selected " + statusValue)
      done()
    })

    /**
     * click on detail view
     */
    test(`(${browserType}):click-dashboard-security-family-status-detail-view`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS_DETAIL_VIEW
      )
    })

    /**
     * logout
     */
    test(`(${browserType}):logout`, async (done) => {
      Utils.logout(page, done, tag)
    })
  })

}
