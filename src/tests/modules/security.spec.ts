import { BrowserTypes } from "../../browsers"
import { PlayWrightHelper } from "../../helpers"
import { Utils } from "./common"
import * as Constants from "../.././constants"

const playwright = require("playwright")
const fs = require("fs")

const data = JSON.parse(fs.readFileSync("data.json"))
const tag = JSON.parse(fs.readFileSync("tags.json"))

// Loop over all the supported browsers
for (const browserType of BrowserTypes) {
  describe(`(${browserType}):security`, () => {
    let PAGE_URL = data["environment"]
    let browser = null
    let page = null
    /**
     * Create the browser and page context
     */
    beforeAll(async () => {
      browser = await playwright[browserType].launch({
        headless: data["headless"],
        slowMo: data["delay"],
      })
      page = await browser.newPage()

      if (!page) {
        throw new Error("Connection wasn't established")
      }

      // Open the page
      await page.goto(PAGE_URL, {
        waitUntil: "networkidle0",
      })
    }, 50000)
    /**
     * Runs after each test
     */
    beforeEach(async () => {
      //await PlayWrightHelper.screenshot(jasmine, page, null, "-beforeEach")
    })

    /**
     * Runs after each test
     */
    afterEach(async () => {
      await PlayWrightHelper.screenshot(jasmine, page, null, "")
    })

    afterAll(async () => {
      await browser.close()
    })

    /**
     * login to the application
     */
    test(`(${browserType}):login`, async (done) => {
      Utils.login(page, done, tag, data)
    })
    /**
     * click on security on the navigation panel
     */
    test(`(${browserType}):click-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY)
    })

    // dashboard starts

    /**
     * click on security dashboard
     */
    test(`(${browserType}):click-dashboard-security`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_DASHBOARD)
    })

    /**
     * click on Control Family Status
     */

    test(`(${browserType}):click-dashboard-security-control-family-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS
      )
    })

    /**
     * click on security Dashboard control risk
     */
    test(`(${browserType}):click-dashboard-security-control-risk`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTROL_RISK
      )
    })

    /**
     * click on security Dashboard control summary
     */
    test(`(${browserType}):click-dashboard-security-control-summary`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTROL_SUMMARY
      )
    })

    /**
     * click on dashboard security exposure
     */
    test(`(${browserType}):click-dashboard-security-security-exposure`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_SECURITY_EXPOSURE
      )
    })

    /**
     * click on dashboard enabling assets
     */
    test(`(${browserType}):click-dashboard-security-enabling-assets`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_ENABLING_ASSET
      )
    })

    /**
     * click on dashboard In-Scope asset
     */

    test(`(${browserType}):click-dashboard-security-In-Scope-assets`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_IN_SCOPE_ASSET
      )
    })

    /**
     * click on dashboard Budget Status Filter
     */
    test(`(${browserType}):click-dashboard-security-budget-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_BUDGET_STATUS_FILTER
      )
    })

    /**
     * click on Dashboard Budget Detail View
     */
    test(`(${browserType}):click-dashboard-security-budget-detail-view`, async (done) => {
      PlayWrightHelper.elementClick(page, done,Constants.SECURITY_DASHBOARD_BUDGET_DETAIL_VIEW)
    })

    /**
     * click on cross for budget detail view- leads to chart view
     */

    test(`(${browserType}):click-dashboard-security-budget-chart-view`, async (done) => {
      PlayWrightHelper.elementClick(page, done,Constants.SECURITY_DASHBOARD_BUDGET_CHART_VIEW)
    })

    /**
     * click on Dashboard Contract Status Filter
     */
    test(`(${browserType}):click-dashboard-security-contract-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER
      )
    })

    /**
     * click on Dashboard Eol Category Filter
     */
    test(`(${browserType}):click-dashboard-security-eol-category-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_EOL_CATEGORY_FILTER
      )
    })

    /**
     * click on Dashboard Eol Status Filter
     */

    test(`(${browserType}):click-dashboard-security-eol-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_EOL_STATUS_FILTER
      )
    })

    /**
     * click on Dashboard Collaboration Category Filter
     */

    test(`(${browserType}):click-dashboard-security-collaboration-category-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER
      )
    })

    /**
     * click on detail view
     */
    test(`(${browserType}):click-dashboard-security-family-status-detail-view`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_DASHBOARD_FAMILY_STATUS_DETAIL_VIEW
      )
    })

    //Dashboard ends

    //Roadmap starts

    /**
     * click on roadmap in security panel dropdown
     */

    test(`(${browserType}):click-roadmap-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(page, done, Constants.SECURITY_ROADMAP)
    })

    /**
     * click on security roadmap status filter
     */
    test(`(${browserType}):click-roadmap-status-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_STATUS_FILTER
      )
      
    })

    /**
     * click on security roadmap year filter
     */
    test(`(${browserType}):click-roadmap-year-filter`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_YEAR_FILTER
      )
    })
    /**
     * click on security roadmap refresh
     */
    test(`(${browserType}):click-roadmap-refresh`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_REFRESH
      )
    })

    /**
     * Click on roadmap multi select
     */

    test(`(${browserType}):click-roadmap-Multi-Select`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_ROADMAP_MULTI_SELECT
      )
    })

    // Roadmap Ends

    //Program Begins

    /**
     * click on the program in security side panel drop down
     */

    test(`(${browserType}):click-program-security-side-panel`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM
      )
    })


    /**
     * click program detailed view
     */

    test(`(${browserType}):click-programlist-program`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_LIST
      )
    })

    /**
    * click program new
    */

    test(`(${browserType}):click-program-new`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_NEW
      )
    })

    /**
    * click program plan name
    */

    test(`(${browserType}):click-program-plan-name`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_NAME
      )
    })

    /**
    * click program plan description
    */

    test(`(${browserType}):click-program-plan-description`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DESC
      )
    })

    /**
    * click program plan owner
    */

    test(`(${browserType}):click-program-plan-owner`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_OWNER
      )
    })

    /**
    * click program plan PM
    */

    test(`(${browserType}):click-program-plan-PM`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PM
      )
    })

    /**
    * click program plan assignee
    */

    test(`(${browserType}):click-program-plan-assignee`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_ASSIGNEE
      )
    })

    /**
    * click program plan category
    */

    test(`(${browserType}):click-program-plan-category`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_CATEGORY
      )
    })

    /**
    * click program plan cross functional team
    */

    test(`(${browserType}):click-program-plan-cross_funtional_team`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_CFT
      )
    })

    /**
    * click program plan status
    */

    test(`(${browserType}):click-program-plan-status`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_STATUS
      )
    })

    /**
    * click program plan type
    */

    test(`(${browserType}):click-program-plan-type`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_TYPE
      )
    })

    /**
    * click program plan priority
    */

    test(`(${browserType}):click-program-plan-priority`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PRIORITY
      )
    })

    /**
    * click program plan size
    */

    test(`(${browserType}):click-program-plan-size`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_SIZE
      )
    })

    /**
    * click program plan Planned Start Date
    */

    test(`(${browserType}):click-program-plan-PSD`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PSD
      )
    })

    /**
    * click program plan Planned End Date
    */

    test(`(${browserType}):click-program-plan-PED`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_PED
      )
    })

    /**
    * click program plan duration 
    */

    test(`(${browserType}):click-program-plan-duration`, async (done) => {
      PlayWrightHelper.elementClick(
        page,
        done,
        Constants.SECURITY_PROGRAM_PLAN_DURATION
      )
    })

    // program ends

    /**
     * logout
     */
    test(`(${browserType}):logout`, async (done) => {
      Utils.logout(page, done, tag)
    })
  })
}
