

import { BrowserTypes } from "../../browsers"
import { PlayWrightHelper } from "../../helpers"
import { Utils } from "./common"
import * as Constants from "../../constants"

const playwright = require("playwright")
const fs = require("fs")


// Loop over all the supported browsers
for (const browserType of BrowserTypes) {
  describe(`(${browserType}):security`, () => {
    let PAGE_URL = 'http://localhost:4200/'
    let browser = null
    let page = null
    /**
     * Create the browser and page context
     */
    beforeAll(async () => {
      browser = await playwright[browserType].launch({
        headless: true,
        slowMo: 100,
      })
      page = await browser.newPage()

      if (!page) {
        throw new Error("Connection wasn't established")
      }

      // Open the page
      await page.goto(PAGE_URL, {
        waitUntil: "networkidle0",
      })
    }, 50000)
    /**
     * Runs after each test
     */
    beforeEach(async () => {
      //await PlayWrightHelper.screenshot(jasmine, page, null, "-beforeEach")
    })

    /**
     * Runs after each test
     */
    afterEach(async () => {
      await PlayWrightHelper.screenshot(jasmine, page, null, "")
    })

    afterAll(async () => {
      await browser.close()
    })

   
    /**
     * click on security roadmap status filter
     */
    test(`(${browserType}):trial-dropdown`, async (done) => {
      await page.click('#trial')
      const statusFilter = await page.$eval('#trial',element=>{

      })
      console.log(statusFilter)
      done()
      
    })


    
  })
}




