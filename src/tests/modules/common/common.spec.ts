import { PlayWrightHelper } from "../../../helpers"
import * as Constants from "../../../constants"
export class Utils {
  /**
   *
   * @param page
   * @param done
   * @param tag
   * @param data
   */
  public static async login(page, done, tag, data) {
    await page.fill(Constants.LOGIN_EMAIL, data["login"]["email"])
    await page.fill(Constants.LOGIN_PASSWORD, data["login"]["password"])
    await page.fill(Constants.COMPANY_CODE, data["login"]["company"])
    PlayWrightHelper.elementClick(page, done, Constants.LOGIN_BUTTON)
  }
  /**
   *
   * @param page
   * @param done
   * @param tag
   */
  public static async logout(page, done, tag) {
    PlayWrightHelper.elementClick(page, done, Constants.LOGOUT)
  }
}
