/**
 * login
 */
export const LOGIN_EMAIL = "#loginUser"
export const LOGIN_PASSWORD = "#loginPassword"
export const COMPANY_CODE = "#companyCode"
export const LOGIN_BUTTON = "#loginButton"

/**
 * navigation
 */

//Security
export const SECURITY = "#security"
export const SECURITY_DASHBOARD = "#securityDashboard"
export const SECURITY_ROADMAP = "#securityRoadmap"
export const SECURITY_PROGRAM = "#securityProgram"
export const SECURITY_EXPOSURE = "#securityExposure"
export const SECURITY_THREAT = "#securityThreat"
export const SECURITY_VULNERABLITY = "#securityVulnerability"
export const SECURITY_INCIDENT = "#securityIncident"

//Assets
export const ASSET = "#asset"
export const ASSET_DASHBOARD = "#assetDashboard"
export const ASSET_DETAIL = "#assetDetail"
export const ASSET_REPORT = "#assetReport"
export const ASSET_EXPORT_ALL_IT = "#assetExportAllIt"
export const ASSET_EXPORT_ALL_APPS = "#assetExportAllApps"
export const ASSET_EXPORT_ALL_PPL = "#assetExportAllPpl"
export const ASSET_DUAL = "#assetDual"
export const ASSET_PPL_USAGE = "#assetPplUsage"
export const ASSET_DUPLICATE_IT = "#assetDuplicateIt"

//Capability
export const CAPABILITY = "#capability"
export const CAPABILITY_MGMT = "#capabilityMgmt"

//Finance
export const FINANCE = "#finance"
export const FINANCE_DASHBOARD = "#financeDashboard"
export const FINANCE_BUDGET = "#financeBudget"
export const FINANCE_VENDOR = "#financeVendor"
export const FINANCE_REPORT = "#financeReport"
export const FINANCE_DETAIL_PARTY_REL = "#financeDetailPartyRel"
export const FINANCE_PARTY_SPENDS = "#financePartySpends"
export const FINANCE_VENDOR_CONTRACT = "#financeVendorContract"
export const FINANCE_DETAIL_VENDOR_CONTRACT = "#financeDetailVendorContract"
export const FINANCE_OPEX_BUDGET = "#financeOpexBudget"
export const FINANCE_CAPEX_BUDGET = "#financeCapexBudget"
export const FINANCE_COST_STATUS = "#financeCostStatus"

/**
 * Security Dashboard
 */

export const SECURITY_DASHBOARD_FAMILY_STATUS = "#securityDashboardFamilyStatus"
export const SECURITY_DASHBOARD_FAMILY_STATUS_DASHBOARD_VIEW =
  "#securityDashboardFamilyStatusDashboardView"
export const SECURITY_DASHBOARD_FAMILY_STATUS_DETAIL_VIEW =
  "#securityDashboardFamilyStatusDetailView"
export const SECURITY_DASHBOARD_CONTROL_RISK = "#securityDashboardControlRisk"
export const SECURITY_DASHBOARD_CONTROL_SUMMARY =
  "#securityDashboardControlSummary"
export const SECURITY_DASHBOARD_SECURITY_EXPOSURE =
  "#securityDashboardSecurityExposure"
export const SECURITY_DASHBOARD_ENABLING_ASSET =
  "#securityDashboardEnablingAsset"
export const SECURITY_DASHBOARD_IN_SCOPE_ASSET =
  "#securityDashboardInScopeAsset"
export const SECURITY_DASHBOARD_BUDGET_STATUS_FILTER =
  "#securityDashboardBudgetStatusFilter"
export const SECURITY_DASHBOARD_BUDGET_DETAIL_VIEW =
  "#securityDashboardBudgetDetailView"
// close button looks like dynamically created so working it out with xpath
export const SECURITY_DASHBOARD_BUDGET_CHART_VIEW =
  "xpath=/html/body/app-root/app-layout/div/div[2]/app-threat-package/div[3]/div/div/app-landing-dashboard/div/div/div/app-plan-dashboard/kendo-dialog/div[2]/kendo-dialog-titlebar/div[2]/a"
export const SECURITY_DASHBOARD_CONTRACT_STATUS_FILTER =
  "#securityDashboardContractStatusFilter"
export const SECURITY_DASHBOARD_EOL_CATEGORY_FILTER =
  "#securityDashboardEolCategoryFilter"
export const SECURITY_DASHBOARD_EOL_STATUS_FILTER =
  "#securityDashboardEolStatusFilter"
export const SECURITY_DASHBOARD_COLLABARATION_CATEGORY_FILTER =
  "#securityDashboardCollaborationCategoryFilter"

/**
 * Security Roadmap
 */

export const SECURITY_ROADMAP_STATUS_FILTER = "#securityRoadMapStatusFilter"
export const SECURITY_ROADMAP_YEAR_FILTER = "#securityRoadMapYearFilter"
export const SECURITY_ROADMAP_REFRESH = "#securityRoadMapRefresh"
export const SECURITY_ROADMAP_MULTI_SELECT = "#securityRoadMapMultiSelect"

/**
 * Security Program
 */

export const SECURITY_PROGRAM_DASHBOARD = "#securityProgramDashboard"
export const SECURITY_PROGRAM_LIST = "#securityProgramList"
export const SECURITY_PROGRAM_NEW = "#securityProgramNew"
export const SECURITY_PROGRAM_PLAN_NAME = "#securityProgramPlanName"
export const SECURITY_PROGRAM_PLAN_DESC = "#securityProgramPlanDesc"
export const SECURITY_PROGRAM_PLAN_OWNER = "#securityProgramPlanOwner"
export const SECURITY_PROGRAM_PLAN_PM = "#securityProgramPlanPM"
export const SECURITY_PROGRAM_PLAN_ASSIGNEE = "#securityProgramPlanAssignee"
export const SECURITY_PROGRAM_PLAN_CATEGORY = "#securityProgramPlanCategory"
export const SECURITY_PROGRAM_PLAN_CFT = "#securityProgramPlanCFT"
export const SECURITY_PROGRAM_PLAN_STATUS = "#securityProgramPlanStatus"
export const SECURITY_PROGRAM_PLAN_TYPE = "#securityProgramPlanType"
export const SECURITY_PROGRAM_PLAN_PRIORITY = "#securityProgramPlanPriority"
export const SECURITY_PROGRAM_PLAN_SIZE = "#securityProgramPlanSize"
export const SECURITY_PROGRAM_PLAN_PSD = "securityProgramPlanPSD"
export const SECURITY_PROGRAM_PLAN_PED = "#securityProgramPlanPED"
export const SECURITY_PROGRAM_PLAN_DURATION = "#securityProgramPlanDuration"
export const SECURITY_PROGRAM_PLAN_SAVE="#securityProgramPlanSave"
export const SECURITY_PROGRAM_PLAN_CLOSE="#securityProgramPlanClose"
export const SECURITY_PROGRAM_PLAN_EDIT="#securityProgramPlanEdit"
export const SECURITY_PROGRAM_PLAN_COMMENT="#securityProgramPlanComment"
export const SECURITY_PROGRAM_PLAN_DELETE="#securityProgramPlanDelete"
export const SECURITY_PROGRAM_PLAN_DAILOG_SAVE_OK= "#saveConfirmationOk"
export const SECURITY_PROGRAM_PLAN_DAILOG_DELETE_OK= "#deleteConfirmationOk"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_VIEW = "#securityProgramPlanSubplanView"

// Sub plan
export const SECURITY_PROGRAM_PLAN_ROW = "#securityProgramPlanRow"
export const SECURITY_PROGRAM_PLAN_ROW_ADD = "#securityProgramPlanRowAdd"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_NAME = "#securityProgramPlanSubplanName"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_OWNER = "#securityProgramPlanSubplanOwner"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_PM = "#securityProgramPlanSubplanPM"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_ASSIGNEE = "#securityProgramPlanSubplanAssignee"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_CFT = "#securityProgramPlanSubplanCFT"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_STATUS = "#securityProgramPlanSubplanStatus"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_TYPE = "#securityProgramPlanSubplanType"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_PRIORITY = "#securityProgramPlanSubplanPriority"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_PSD = "securityProgramPlanSubplanPSD"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_PED = "#securityProgramPlanSubplanPED"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_DURATION = "#securityProgramPlanSubplanDuration"

export const SECURITY_PROGRAM_PLAN_SUBPLAN_SD = "#securityProgramPlanSubplanSD"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_ED = "#securityProgramPlanSubplanED"

export const SECURITY_PROGRAM_PLAN_SUBPLAN_COMPLETE = "#securityProgramPlanSubplanComplete"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS = "#securityProgramPlanSubplanDetails"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS_OBJECTIVE = "#securityProgramPlanSubplanDetailsObjective"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS_TP = "#securityProgramPlanSubplanDetailsObjectiveTP"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_DETAILS_GUIDANCE = "#securityProgramPlanSubplanDetailsGuidance"

//Subplan options
export const SECURITY_PROGRAM_PLAN_SUBPLAN_CAPABILITY = "#securityProgramPlanSubplanCapability"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_ASSET = "#securityProgramPlanSubplanAsset"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_THREAT = "#securityProgramPlanSubplanThreat"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_VULNERABILITY = "#securityProgramPlanSubplanVulnerablity"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_BUDGET = "#securityProgramPlanSubplanBudget"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_RESOURCE = "#securityProgramPlanSubplanResource"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_COLLABORATION = "#securityProgramPlanSubplanCollaboration"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_JIRA = "#securityProgramPlanSubplanJira"

//Subplan Top options
export const SECURITY_PROGRAM_PLAN_SUBPLAN_EDIT = "#securityProgramPlanSubplanEdit"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_SAVE="#securityProgramPlanSave"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_CLOSE="#securityProgramPlanClose"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_COMMENT="#securityProgramPlanComment"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_DELETE="#securityProgramPlanDelete"

export const SECURITY_PROGRAM_PLAN_SUBPLAN_ROW = "#securityProgramPlanSubplanAdd"
export const SECURITY_PROGRAM_PLAN_SUBPLAN_ROW_ADD = "#securityProgramPlanSubplanRowAdd"





/**
 * logout
 */
export const LOGOUT = "#logoutLink"
